;;; For NDK versions below 19, you need to create a standalone toolchain.

(in-package :cl-user)

(pushnew :android *features*)
(pushnew :aarch64 *features*)

(require :cmp)

(defvar *ndk-toolchain* (ext:getenv "ANDROID_NDK_TOOLCHAIN"))
(defvar *ecl-android*   (ext:getenv "ECL_ANDROID"))

(setf c::*ecl-include-directory* (x:cc *ecl-android* "/include/")
      c::*ecl-library-directory* (x:cc *ecl-android* "/lib/"))

(defun ecl-config (flags)
  (read-line (ext:run-program (x:cc *ecl-android* "/bin/ecl-config")
                              (list flags))))

(setf c::*cc*              (let* ((%path (x:cc *ndk-toolchain* "/bin/aarch64-linux-android~A-clang"))
                                  (path (or (probe-file (format nil %path 21)) ; >= ndk-19
                                            (probe-file (format nil %path "")) ; <= ndk-18
                                            (error "clang compiler not found"))))
                             (namestring path))
      c::*ld*              (x:cc *ndk-toolchain* "/bin/aarch64-linux-android-ld")
      c::*ar*              (x:cc *ndk-toolchain* "/bin/aarch64-linux-android-ar")
      c::*ranlib*          (x:cc *ndk-toolchain* "/bin/aarch64-linux-android-ranlib")
      c::*cc-flags*        (x:cc (ecl-config "--cflags")
                                 " -DANDROID -DPLATFORM_ANDROID -O2 -fPIC -fno-common -D_THREAD_SAFE -I"
                                 *ecl-android* "/build/gmp")
      c::*ld-flags*        (x:cc "-L" *ecl-android* "/lib -lecl -ldl -lm "
                                 "-L" *ndk-toolchain* "/sysroot/usr/lib/aarch64-linux-android/")
      c::*ld-rpath*        nil
      c::*ld-shared-flags* (x:cc "-shared " c::*ld-flags*)
      c::*ld-bundle-flags* c::*ld-shared-flags*)
      
(format t "~%*** cross compiling for 'aarch64' ***~%")
