
Prepare
-------

You need to prepare your android development environment first, as described
in [README-PREPARE](../../README-PREPARE.md).

(see also [known issues](http://wiki.qt.io/Qt_for_Android_known_issues))

In order to have all external resources, like qml files and images, included
in the executable, you need to add them to the resource file (already done
here, see `qtapp.qrc`).



Test build (optional)
---------------------

(build and run a desktop version)

You might want to verify if the app would run on the desktop.

(Or you just wanted to see the slight differences between both builds.)

```
  eql5 make-desktop.lisp
  qmake tic_tac_toe_desktop.pro
  make
```

Now you should be able to run:

```
  ./tic_tac_toe_desktop
```

To verify if all resources have been included, you need to move it to another
directory and run it from there.



Build (cross-compile)
--------------------

```
  ./1-copy-libs.sh              # copy EQL5 and ECL libs

  ecl-android -shell make.lisp  # note 'ecl-android'
  qmake-android tic_tac_toe.pro # note 'qmake-android'
  make
```

We use a trick for cross-compiling: since we don't want an EQL5 version only
for cross-compiling, linked to a 32 bit ECL, we pretend to be the `eql5`
executable, by defining all packages, symbols and macros(!) of EQL5, and
defining dummy functions for all EQL5 functions, so the compiler will not
complain.

This is done by loading the file `../../utils/EQL5-symbols.lisp` into ECL.

There is only one drawback here: we can't use reader macros containing EQL5
code (except for Qt enums); see e.g. file `lisp/qml-lisp.lisp`, which has them
removed, compared to the desktop version (see EQL5 QML examples).



Build APK file
--------------

The android SDK tools are expected to be installed in `/opt/android/sdk`, so
it's a good idea to move them there (Android Studio for example will install
them by default in `~/Android/Sdk` during the first start of Android Studio).

There is a nice Qt utility, which will automate the whole (and complex) build
process.

Run the tool using the second script:

```
  ./2-build-apk.sh
```

This will run the following command:

```
  androiddeployqt --input android-libtic_tac_toe.so-deployment-settings.json \
                  --output android-build \
                  --gradle \
                  --install \
                  --verbose
```

The mentioned `json` file is generated automatically during `make`, see above.

Now the build should succeed (verbosely, as requested). The APK package can be
found in `android-build/build/outputs/apk/`; it will be installed automatically
if you add option `--install` to `androiddeployqt` (like above).



Debug
-----

Of course debugging is best done using the desktop version (where possible).

If you want to use logging on the device through `adb logcat`, you can use:

```
  ;; Lisp
  (eql:qlog "message")
  (eql:qlog 1 "plus" 2 "gives" 6/2)
  (eql:qlog "x: ~A y: ~A" x y)

  // QML
  console.log("message")
```

The logged lines will contain `[EQL5]`, so you can use:

```
  adb logcat -s "[EQL5]"
```



### Desktop notes

To run it on the desktop, do:
```
  eql5 run.lisp
```

Using Slime (see docu in desktop EQL5), first run
```
  eql5 ~/slime/eql-start-swank.lisp run.lisp
```
then connect from Emacs `M-x slime-connect`
