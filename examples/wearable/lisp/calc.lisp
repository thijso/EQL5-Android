(defpackage :calculator
  (:nicknames :clc)
  (:use :common-lisp :eql :qml)
  (:export
   #:back-clicked
   #:clear-clicked
   #:digit-clicked
   #:equal-clicked
   #:function-clicked
   #:point-clicked
   #:operation-clicked
   #:reci-clicked
   #:sign-clicked
   #:words-clicked))

(in-package :clc)

(defvar *precision* 0f0) ; f = float, d = double, l = long
(defvar *value1*    0)
(defvar *value2*    nil)
(defvar *reset*     nil)
(defvar *decimals*  nil)
(defvar *operation*)

(defun error-to-string (err)
  (let ((str (string-trim "#<a >" (write-to-string err :case :downcase))))
    (x:if-it (search "0x" str :from-end t)
             (subseq str 0 x:it)
             str)))

(defun funcall-protect (fun &rest args)
  (multiple-value-bind (val err)
      (ignore-errors (apply fun args))
    (or val
        (progn
          (qmsg (error-to-string err))
          0))))

(defun display-number (n)
  (flet ((str (x)
           (let ((str (format nil "~:D" x)))
             (when (<= (length str) 14)
               str))))
    (x:when-it (funcall-protect (lambda (x) (float x *precision*)) n)
      (q> |text| ui:*float* (princ-to-string x:it))
      (let ((s-num (str (numerator n)))
            (s-den (str (denominator n))))
        (q> |text| ui:*real-num* (or (and s-den s-num) "-"))
        (q> |text| ui:*real-den* (or (and s-num s-den) "-")))
      (q> |enabled| ui:*blah* (= 1 (denominator n))))))

(defun clear-display ()
  (setf *value1* 0
        *decimals* nil)
  (display-number 0))

(defun words-clicked ()
  (qmsg (format nil "~R" *value1*)))

(defun digit-clicked (digit)
  (when *reset*
    (clear-display)
    (setf *reset* nil))
  (setf *value1* (if *decimals*
                     (+ (* digit (expt 10 (- (incf *decimals*))))
                        *value1*)
                     (+ digit
                        (* 10 *value1*))))
  (display-number *value1*))

(defun back-clicked ()
  (when (and *decimals* (zerop *decimals*))
    (setf *decimals* nil))
  (setf *value1* (if *decimals*
                     (let ((n (expt 10 (decf *decimals*))))
                       (/ (truncate (* n *value1*)) n))
                     (truncate (/ *value1* 10))))
  (display-number *value1*))

(defun invert (operation)
  (setf *value1* (funcall-protect operation *value1*))
  (display-number *value1*))

(defun sign-clicked ()
  (invert '-))

(defun reci-clicked ()
  (invert '/))

(defun point-clicked ()
  (setf *decimals* 0))

(defun clear-clicked ()
  (setf *value2* nil)
  (clear-display))

(defun operate ()
  (x:when-it (funcall-protect *operation* *value2* *value1*)
    (setf *value2* x:it)
    (display-number *value2*)))

(defun operation-clicked (fun)
  (if *value2*
      (operate)
      (setf *value2* *value1*))
  (setf *operation* fun
        *reset* t))

(defun function-clicked (fun)
  (when (eql 'pi fun)
    (setf *value1* pi
          fun      'identity))
  (x:when-it (funcall-protect fun *value1*)
    (if (complexp x:it)
        (qmsg (format nil "Can't display complex number:~%~A" x:it))
        (progn
          (setf *value1* (rationalize x:it)
                *value2* nil
                *reset*  t)
          (display-number *value1*)))))

(defun equal-clicked ()
  (when *value2*
    (operate)
    (shiftf *value1* *value2* nil)
    (setf *reset* t)))
