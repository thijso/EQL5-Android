import QtQuick 2.10

Text {
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.Text.AlignVCenter
    padding: 2
    font.pixelSize: 20
    font.bold: true
    color: "white"
}
