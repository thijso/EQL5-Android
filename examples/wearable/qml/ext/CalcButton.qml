import QtQuick 2.10
import QtQuick.Controls 2.10
import EQL5 1.0

Button {
    width: 38
    height: width
    font.pixelSize: 24
    font.bold: true

    background: Rectangle {
        radius: width / 2
        color: parent.pressed ? "darkgray" : "lightskyblue"
    }

    onClicked: Lisp.call("watch:button-clicked", text, "lightskyblue")
}
