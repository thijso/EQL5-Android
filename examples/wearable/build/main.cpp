#undef SLOT

#include "main.h"
#include <ecl/ecl.h>
#include <eql5/eql.h>
#include <QTextCodec>
#include <QFileInfo>
#include <QLabel>

extern "C" {
    void ini_app(cl_object);
}

int main(int argc, char** argv) {

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    EventApplication qapp(argc, argv);
    //qApp->setOrganizationName("MyTeam");
    //qApp->setOrganizationDomain("my-team.org");
    qApp->setApplicationName(QFileInfo(qApp->applicationFilePath()).baseName());

    CBridge c_bridge(&qapp, "c_bridge");

    // splash pixmap (see "../../../img/logo-watch.png")
    QLabel* splash = new QLabel;
    splash->setPixmap(QPixmap(":/img/logo-watch.png"));
    splash->setAlignment(Qt::AlignCenter);
    splash->show();
    qApp->processEvents();
    splash->deleteLater();

    QTextCodec* utf8 = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(utf8);

    EQL eql;
    // we need a fallback restart for connections from Slime
    eql.exec(ini_app, "(loop (with-simple-restart (restart-qt-events \"Restart Qt event processing.\") (qexec)))");

    return 0; }
