(in-package :heart-rate)

(qrequire :quick)

(defun start ()
  (qml:ini-quick-view "qml/main.qml")
  (qconnect qml:*quick-view* "statusChanged(QQuickView::Status)" ; for reloading
            (lambda (status)
              (case status
                (#.|QQuickView.Ready|
                 (qml-reloaded))
                (#.|QQuickView.Error|
                 (qmsg (x:join (mapcar '|toString| (|errors| *quick-view*))
                               #.(make-string 2 :initial-element #\Newline)))))))
  (qconnect (qapp) "quitApp()" 'qquit) ; see hack in '../build/main.h'
  (qlater 'keep-screen-on)
  (qsingle-shot 1000 'load-ip)
  (run))

(defun set-reload-url (ip.4)
  (setf *reload-url* (format nil "http://192.168.1.~A:8080/" ip.4)))

(defun save-ip ()
  (x:when-it (q< |currentIndex| ui:*remote-ip*)
    (with-open-file (s "ip.4" :direction :output :if-exists :supersede)
      (write-line (princ-to-string x:it) s))
    (set-reload-url x:it)))

(defvar *reload-url* nil)

(defun load-ip ()
  (set-reload-url (if (probe-file "ip.4")
                      (with-open-file (s "ip.4")
                        (let ((str (read-line s)))
                          (q> |currentIndex| ui:*remote-ip*
                              (or (ignore-errors (parse-integer str)) 0))
                          str))
                      "0")))

(defun set-url (url)
  (when url
    (setf *reload-url* url)))

;;; load remote Lisp file

(let ((ini t))
  (defun eql::load* (file &optional url)
    "Load Lisp file from an url."
    (set-url url)
    (when ini
      (setf ini nil)
      (load "curl"))
    (let ((remote-file (x:cc *reload-url* file (if (pathname-type file) "" ".lisp"))))
      (load (make-string-input-stream (qfrom-utf8 (eql::curl remote-file))))
      remote-file)))

(export 'eql::load* :eql)

;;; reload QML from remote

(defun reload-qml (&optional url)
  "Reload QML file(s) from an url."
  (set-url url)
  (let ((src (|toString| (|source| qml:*quick-view*))))
    (if (x:starts-with "qrc:/" src)
        (|setSource| qml:*quick-view* (qnew "QUrl(QString)"
                                            (x:string-substitute *reload-url* "qrc:/" src)))
        (qml:reload))
    (|toString| (|source| qml:*quick-view*))))

(defun qml-reloaded ()
  ;; re-ini
  )
