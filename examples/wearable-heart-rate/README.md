### Prepare

Since the heart rate sensor is not (yet) supported in QML, you need to apply a
patch to the Qt sources, please see directory `qt-sensor-hack/`.

Enable the developer options on your watch, and enable debugging over WiFi, and
connect it to your local WiFi.



### Build it

```
  ./1-copy-libs.sh

  ecl-android-32 --shell make.lisp
  qmake-android-32 wearable.pro
  make
```

Edit `connect.sh` and substitute `?` with the last number of your watch IP (as
shown in the WiFi connection menu on the watch).

```
  ./connect.sh

  ./2-build-apk.sh
```

You'll need to confirm the connection request on your watch.

If the connection script doesn't work the first time (or during deployment, see
below), just try again, or run `adb kill-server` prior to connect. You may also
(sometimes) need to turn off/on your WiFi on the watch.

The first time you run `2-build-apk.sh` it may take up to several minutes to
deploy your app to the watch through WiFi (just try to be patient...).



### Usage

Since swiping the app to the right will close the app, we use swiping up and
down to switch between the 2 pages of this app.

Page 2 is for running Swank and reloading Lisp and QML files (see examples
**my** and **REPL**).

Starting Swank (especially the first time) will take a while; after starting,
it will show the IP to connect to from the PC.

You need to complete the IP of your PC WiFi address (see 'tumbler' widget) and
run `./web-server.sh` in order to be able to load Lisp files from the PC (see
`load*`) and reload QML files (see command `:r`).



### Desktop

To run it on the desktop, do (font sizes will be different):
```
  eql5 run.lisp
```

To develop on the desktop, run the following, then connect from Slime:
```
  eql5 ~/slime/eql-start-swank.lisp run.lisp
```



### Important note

The animation is nice to watch (but not to the watch!), because it's a perfect
example of what to absolutely **AVOID** on a wearable device.

Here the output of `adb shell` followed by `top` (after running `connect.sh`):

```
%CPU   %MEM  ARGS
18.0   8.7   org.qtproject.example.heartrate
18.3   1.6   surfaceflinger
 9.6   0.5   android.hardware.graphics.composer@2.1-service
```

As you can see, the animation sums up to almost **50%** of CPU consumption,
which will (of course) drain the watch battery like crazy...; this is on a
recent (as in 2020) Wear OS 2 watch.

For comparison: the wearable calculator example from the parent directory
has exactly **0%** CPU consumption when idle, and the other 2 processes listed
above behave almost the same (with idle peak levels of neglectable 0.3%).

