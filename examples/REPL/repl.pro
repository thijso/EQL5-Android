QT             = widgets printsupport uitools quick quickwidgets qml network androidextras
TEMPLATE       = app
CONFIG         += plugin no_keywords release
ECL_ANDROID    = $$(ECL_ANDROID)
INCLUDEPATH    += $$ECL_ANDROID/include ../../include
LIBS           += -L$$ECL_ANDROID/lib -lecl -L./build -lapp -L../../lib -leql5
PRE_TARGETDEPS += build/libapp.a
TARGET         = repl
DESTDIR        = ./android-build/libs/arm64-v8a
OBJECTS_DIR    = ./tmp/
MOC_DIR        = ./tmp/

HEADERS        += build/main.h
SOURCES        += build/main.cpp

RESOURCES      = repl.qrc

ANDROID_PACKAGE_SOURCE_DIR = android-sources
