#ifndef LOAD_H
#define LOAD_H

#include <QtAndroid> 
#include <QDateTime>
#include <QEventLoop>
#include <QWidget>
#include <QPainter>

QT_BEGIN_NAMESPACE

class CBridge : public QObject {
    Q_OBJECT
public:
    CBridge(QObject*, const QString&);

    Q_INVOKABLE bool checkPermission(const QString& name) {
        // e.g. "android.permission.WRITE_EXTERNAL_STORAGE"
        // this handles the new Android permission model, starting with API version 23 (Android 6),
        // that is: we need to ask for the permission at runtime (every time we need a permission)
        bool granted = true;
#if QT_VERSION > 0x050A00 // 5.10
        QtAndroid::PermissionResult res = QtAndroid::checkPermission(name);
        if(res == QtAndroid::PermissionResult::Denied) {
            // we use our own sync call because requestPermissionsSync() may hang
            QEventLoop loop;
            QtAndroid::requestPermissions(QStringList() << name, [&](const QtAndroid::PermissionResultMap& res) {
                granted = (res[name] == QtAndroid::PermissionResult::Granted);
                loop.exit(); } );
            loop.exec(QEventLoop::ExcludeUserInputEvents); }
#endif
        return granted; }

    Q_INVOKABLE void restartApp() {
        // stolen from: https://www.kdab.com/qt-on-android-how-to-restart-your-application/
        // this will restart the app after a few seconds (not immediately)
        // (uses CBridge, Java Native Interface)

        auto activity = QtAndroid::androidActivity();

        auto packageManager = activity.callObjectMethod(
            "getPackageManager", "()Landroid/content/pm/PackageManager;");
         
        auto activityIntent = packageManager.callObjectMethod(
            "getLaunchIntentForPackage", "(Ljava/lang/String;)Landroid/content/Intent;",
            /*L*/ activity.callObjectMethod("getPackageName", "()Ljava/lang/String;").object());
         
        auto pendingIntent = QAndroidJniObject::callStaticObjectMethod(
            "android/app/PendingIntent",
            "getActivity", "(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;",
            /*L*/  activity.object(),
            /*IL*/ jint(0), activityIntent.object(),
            /*I*/  QAndroidJniObject::getStaticField<jint>("android/content/Intent", "FLAG_ACTIVITY_CLEAR_TOP"));
         
        auto alarmManager = activity.callObjectMethod(
            "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;",
            /*L*/ QAndroidJniObject::getStaticObjectField("android/content/Context", "ALARM_SERVICE", "Ljava/lang/String;").object());
         
        alarmManager.callMethod<void>(
            "set", "(IJLandroid/app/PendingIntent;)V",
            /*IJL*/ QAndroidJniObject::getStaticField<jint>("android/app/AlarmManager", "RTC"),
                    jlong(QDateTime::currentMSecsSinceEpoch() + 1000), // wait 1 sec (for slow devices)
                    pendingIntent.object()); }
};

class Splash : public QWidget {
    Q_OBJECT
public:
    Splash(const QPixmap& pix, const QColor& col = Qt::black) { pixmap = pix; color = col; }

    void paintEvent(QPaintEvent*) {
        QPainter p(this);
        p.fillRect(rect(), color);
        // use this for full screen portrait splash
        //QRect r;
        //if(width() > height()) {
        //    float w = height() * pixmap.width() / pixmap.height();
        //    r = QRect((width() - w) / 2, 0, w, height()); }
        //else {
        //    r =  rect(); }
        QRect r(pixmap.rect());
        r.moveCenter(rect().center());
        p.drawPixmap(r, pixmap); }

    QPixmap pixmap;
    QColor color;
};

QT_END_NAMESPACE

#endif
