;;;
;;; Includes everything for Quicklisp and Swank.
;;; Requires 'assets/lib/*' to contain all precompiled ECL contribs.
;;;

(si::trap-fpe t nil) ; ignore floating point exceptions

(in-package :eql)

(defvar *assets-lib* "assets:/lib/")

(defun copy-asset-files (dir-name)
  "Copy all files from APK 'assets/lib/' to home path."
  (flet ((trim (name)
           (if (x:starts-with *assets-lib* name)
               (subseq name (length *assets-lib*))
               name)))
    (qlet ((dir "QDir(QString)" dir-name))
      (ensure-directories-exist (trim (x:cc dir-name "/")))
      (dolist (info (|entryInfoList| dir))
        (if (|isDir| info)
            (copy-asset-files (|filePath| info))
            (let* ((from (|filePath| info))
                   (to (trim from)))
              (cond ((probe-file to)
                     (delete-file to))
                    ((string= "eclrc" to)
                     (setf to (x:cc "." to))))
              (unless (or (probe-file to) ; e.g. ".eclrc"
                          (|copy.QFile| from to))
                (qmsg (format nil "Error copying asset file: ~S" from))
                (return-from copy-asset-files)))))))
  t)

(defun touch-file (name)
  (open name :direction :probe :if-does-not-exist :create))

(defun delayed-eval (msec string)
  (qsingle-shot msec (lambda () (funcall (sym 'eval* :editor) string))))

;; will be updated on every ECL/EQL5 upgrade
;; N.B. always increment by 2, so 32bit will be odd and 64bit even
(defconstant +app-version+ (+ 25 #+aarch64 1))

(let ((file ".app-version"))
  (defun app-version ()
    (if (probe-file file)
        (with-open-file (s file)
          (let ((str (make-string (file-length s))))
            (read-sequence str s)
            (values (parse-integer str))))
        0))
  (defun save-app-version ()
    (with-open-file (s file :direction :output :if-exists :supersede)
      (format s "~D" +app-version+))
    (values)))

(defun post-install ()
  (x:when-it (probe-file "profile.fas")
    (delete-file x:it))                 ; might not be available (in update)
  (when (copy-asset-files *assets-lib*)
    (shell "chmod 664 .eclrc")
    (shell "chmod 664 settings/*.lisp")
    ;; chmod -R would give 'permission denied'
    (shell "chmod 664 examples/*.lisp")
    (shell "chmod 664 examples/Qt/*.lisp")
    (shell "chmod 664 examples/QML/Draw/*.lisp")
    (let ((*standard-output* nil))
      (ignore-errors (load ".eclrc")))  ; don't hang on startup
    (delayed-eval 1000 "(help t)")
    :done))

(defun ini ()
  (setf *standard-input* *query-io*)                   ; will use Query Dialog
  (si:install-bytecodes-compiler)
  (x:when-it (probe-file ".eclrc")
    (ignore-errors (load x:it)))                       ; don't hang on startup
  (delayed-eval 0 (if (/= +app-version+ (app-version)) ; both upgrade and downgrade
                      "(and (eql::post-install) (eql::save-app-version))"
                      "(help t)")))

;; Quicklisp setup (stolen from 'ecl-android')

(defun sym (symbol package)
  (intern (symbol-name symbol) package))

(defun quicklisp ()
  (unless (find-package :quicklisp)
    (when (and (require :ecl-quicklisp)
               (require :deflate)
               (require :ql-minitar))
      ;; replace interpreted function with precompiled one from DEFLATE
      (setf (symbol-function (sym 'gunzip :ql-gunzipper))
            (symbol-function (sym 'gunzip :deflate)))))
  :quicklisp)

(export 'quicklisp)

;; Swank setup (stolen from 'ecl-android')

(defun swank/create-server (interface port dont-close style)
  (funcall (sym 'create-server :swank)
           :interface interface
           :port port
           :dont-close dont-close
           :style style))

(defun start-swank (&key (interface "0.0.0.0") log-events
                      (load-contribs t) (setup t) (delete t) (quiet t)
                      (port 4005) (dont-close t) (style :spawn))
  (unless (find-package :swank)
    (require :asdf)
    (funcall (sym 'load-system :asdf) :swank))
  (funcall (sym 'init :swank-loader)
           :load-contribs load-contribs
           :setup setup
           :delete delete
           :quiet quiet)
  (setf (symbol-value (sym '*log-events* :swank)) log-events)
  (eval (read-from-string "(swank/backend:defimplementation swank/backend:lisp-implementation-program () \"org.lisp.ecl\")"))
  (if (eql :spawn style)
      (swank/create-server interface port dont-close style)
      (mp:process-run-function
       "SLIME-listener"
       (lambda () (swank/create-server interface port dont-close style)))))

(defun stop-swank ()
  (when (find-package :swank)
    (funcall (sym 'stop-server :swank) 4005)
    :stopped))

(export 'start-swank)
(export 'stop-swank)

#+release
(defvar *c-bridge* (qfind-child (qapp) "c_bridge"))

;; permissions (android api >= 23)

(defun ensure-android-permission (&optional (name "android.permission.WRITE_EXTERNAL_STORAGE"))
  "Check/request Android permission (for API level >= 23); the name defaults to \"android.permission.WRITE_EXTERNAL_STORAGE\". Returns T on granted permission."
  (! "checkPermission" (:qt *c-bridge*) name)) ; see '../build/main.h'

(export 'ensure-android-permission)

;; shell

(defvar *shell-output* nil)

(export '*shell-output*)

(defun shell (command)
  "Run shell commands; example:
  (shell \"df -h\")"
  (let ((tmp "tmp.txt"))
    (with-open-file (s tmp :direction :output :if-exists :supersede)
      (ext:run-program "sh" (list "-c" command)
                       :output s)) ; we need a file stream here
    (with-open-file (s tmp)
      (let ((str (make-string (file-length s))))
        (read-sequence str s)
        (fresh-line)
        (princ str)
        (setf *shell-output* (x:split (string-trim '(#\Newline) str) #\Newline))))
    (delete-file tmp))
  (values))

(export 'shell)

(defun help (&optional startup)
  (flet ((output (text &optional rich-text)
           (qml:js "output_model" "addBlock(~S, ~S, false, false, ~A)"
                   text
                   (symbol-value (sym '*output-text-color* :editor))
                   (qml:js-arg rich-text))))
    (let* ((mini (and startup (qml:q< |isPhone| nil)))
           (text (if mini
                     (format nil "~% :h for help")
                     (format nil "~% :a  (require :asdf)          ; see asdf:load-system~
                                  ~% :q  (eql:quicklisp)          ; will install/load it~
                                  ~% :s  (eql:start-swank)        ; PC: adb forward tcp:4005 tcp:4005~
                                  ~% :f  (dialogs:get-file-name)  ; see dialogs:*file-name*~
                                  ~% :?  (editor:find-text regex) ; :? prin[1c] ; [Return] for next match~
                                  ~% :c                           ; clear output window~
                                  ~% :k                           ; kill eval thread~
                                  ~% *                            ; copy to clipboard~
                                  ~% (shell \"df -h\")              ; see *shell-output*~
                                  ~%~
                                  ~% * tap and hold to select/copy/paste/eval s-exp (e.g. on 'defun')~
                                  ~% * tap and hold cursor buttons to move to beginning/end of line/file~
                                  ~% * double [Space] for auto-completion (e.g. m-v-b)~
                                  ~%~%"))))
      (output text)
      (unless mini
        (output #.(with-open-file (s "lisp/htm/docu-links.htm")
                    (let ((text (make-string (file-length s))))
                      (read-sequence text s)
                      text))
                t)
        (output (string #\Newline))))) ; ensure links not to be covered by paren buttons
  (values))

(export 'help)

(qlater 'ini)
