import QtQuick 2.13
import QtQuick.Controls 2.13

TextField {
  palette {
    highlight: "#007aff"
    highlightedText: "white"
  }
}
