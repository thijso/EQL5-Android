import QtQuick 2.13
import QtQuick.Controls 2.13
import "." as Ext
import EQL5 1.0

Rectangle {
  id: queryDialog
  objectName: "query_dialog"
  color: "#f0f0f0"
  visible: false

  Column {
    anchors.fill: parent

    Ext.MenuBack {
      id: menuBack
      label: "Query Dialog"

      onPressed: {
        queryInput.clear()
        Lisp.call("dialogs:exited")
      }
    }

    TextField {
      id: queryInput
      objectName: "query_input"
      width: parent.width
      font.family: "Hack"
      inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText

      onAccepted: {
        Lisp.call("dialogs:exited")
        Lisp.call("editor:ensure-output-visible")
      }
    }

    Text {
      objectName: "query_text"
      width: parent.width
      height: main.availableHeight() - menuBack.height - queryInput.height
      leftPadding: 8
      rightPadding: 8
      topPadding: 8
      font.pixelSize: queryInput.font.pixelSize - (main.isPhone ? 2 : 0)
    }
  }

  Row {
    anchors.horizontalCenter: parent.horizontalCenter
    y: queryInput.y + queryInput.height + (main.isPhone ? 7 : 10)
    spacing: 20
    visible: queryInput.focus

    // cursor back
    Ext.ArrowButton {
      opacity: 0.15
      text: "\uf137"

      onPressed:      queryInput.cursorPosition--
      onPressAndHold: queryInput.cursorPosition = 0
    }

    // cursor forward
    Ext.ArrowButton {
      opacity: 0.15
      text: "\uf138"

      onPressed:      queryInput.cursorPosition++
      onPressAndHold: queryInput.cursorPosition = queryInput.length
    }
  }
}
