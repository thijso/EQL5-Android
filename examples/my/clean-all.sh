# Lisp
rm -f *.o                                                     # package.o
find lisp/ -name "*.o" -delete                                # all lisp files
rm build/libapp.a                                             # app lib
find ~/quicklisp/dists/quicklisp/software -name "*.o" -delete # all cross-compiled files

# C++
rm -fr tmp
rm -f Makefile*
rm .qmake.stash

# apk
rm -fr android-build
