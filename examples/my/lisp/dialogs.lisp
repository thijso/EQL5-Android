(defpackage :dialogs
  (:use :cl :eql :qml)
  (:export
   #:load-file
   #:get-file-name
   #:location
   #:*file-name*))

(in-package :dialogs)

(defvar *file-name* nil)
(defvar *callback*  nil)

;; '(js ui:*main* ...)': see JS functions in '../qml/*.qml'

(defun push-dialog (name)
  (js ui:*main* "push~ADialog()" (string-capitalize name)))

(defun pop-dialog ()
  "Pops the currently shown dialog, returning T if there was a dialog to pop."
  (prog1
      (> (q< |depth| ui:*main*) 1)
    (js ui:*main* "popDialog()")
    (exited)))

(defun wait-while-transition ()
  ;; needed for evtl. recursive calls
  (x:while (q< |busy| ui:*main*)
    (qsleep 0.1)))

(let ((exited t))
  (defun wait-for-closed ()
    (setf exited nil)
    (x:while (not  exited)
      (qsleep 0.1)))
  (defun exited () ; called from QML
    (setf exited t)))

;; file browser

(let ((1st t))
  (defun get-file-name (&optional callback)
    #+android
    (ensure-android-permission) ; defaults to 'external storage'
    (|hide| (|inputMethod.QGuiApplication|))
    (when 1st
      (setf 1st nil)
      (set-file-browser-path ":data"))
    (setf *callback* callback)
    ;; force update
    (qlet ((none "QUrl")
           (curr (q< |folder| ui:*folder-model*)))
      (dolist (folder (list none curr))
        (q> |folder| ui:*folder-model* folder)))
    (push-dialog :file)))

(defun directory-p (path)
  (qlet ((info "QFileInfo(QString)" path))
    (|isDir| info)))

(defun set-file-name (file-name) ; called from QML
  (let ((name (remove-if (lambda (ch) (find ch "*?\\")) file-name)))
    (if (directory-p name)
        (set-file-browser-path name)
        (progn
          (pop-dialog)
          (setf *file-name* name)
          (when *callback*
            (funcall *callback*))))))

(defun load-file ()
  (get-file-name 'do-load-file))

(defun do-load-file ()
  (unless (x:empty-string *file-name*)
    (if (probe-file *file-name*)
        (let ((type (pathname-type *file-name*)))
          (when (or (x:starts-with "fas" type)
                    (find type '("lisp" "lsp") :test 'string=))
            (eval::append-output (prin1-to-string (load *file-name*))
                                 eval::*color-values*)))
        (qmsg (format nil "File does not exist:~%~%~S" *file-name*)))))

(defun location (name)
  (cond ((string= ":storage" name)
         #+android "/storage" #-android "/")
        ((string= ":data" name)
         (first (|standardLocations.QStandardPaths| |QStandardPaths.GenericDataLocation|)))
        ((string= ":home" name)
         (namestring *default-pathname-defaults*))))

(defun set-file-browser-path (path) ; called from QML
  (let ((path* (x:cc "file://" (if (x:starts-with ":" path)
                                   (location path)
                                   path))))
    (qlet ((url "QUrl(QString)" (if (x:ends-with "/" path*)
                                    path*
                                    (x:cc path* "/"))))
      (q> |folder| ui:*folder-model* url))))
