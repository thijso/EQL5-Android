QT             = widgets printsupport uitools quick quickwidgets qml multimedia network positioning sensors sql svg webview androidextras
TEMPLATE       = app
CONFIG         += plugin no_keywords release
ECL_ANDROID    = $$(ECL_ANDROID_32)
INCLUDEPATH    += $$ECL_ANDROID/include ../../include
LIBS           += -L$$ECL_ANDROID/lib -lecl -L./build -lapp -L../../lib32 -leql5
PRE_TARGETDEPS += build/libapp.a
TARGET         = my
DESTDIR        = ./android-build/libs/armeabi-v7a
OBJECTS_DIR    = ./tmp/
MOC_DIR        = ./tmp/

HEADERS        += build/main.h
SOURCES        += build/main.cpp

RESOURCES      = my.qrc

ANDROID_PACKAGE_SOURCE_DIR = android-sources

lupdate_only {
    HEADERS = tr.h # only a dummy for translations in Lisp files
}
