# Lisp
rm -f *.o                                                     # package.o
find lisp/ -name "*.o" -delete                                # all lisp files
find ~/quicklisp/dists/quicklisp/software -name "*.o" -delete # all cross-compiled files

# C++
rm -fr tmp
rm -f Makefile*

# apk
rm -fr android-build
