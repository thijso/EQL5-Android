# ECL, EQL5 libs

rm -fr   android-sources/libs/armeabi-v7a
mkdir -p android-sources/libs/arm64-v8a

cp ../../lib/libeql5.so       android-sources/libs/arm64-v8a/
cp ../../lib/libeql5_quick.so android-sources/libs/arm64-v8a/
cp $ECL_ANDROID/lib/libecl.so android-sources/libs/arm64-v8a/

# all prebuilt ECL libs (ASDF etc.)

mkdir -p android-sources/assets/lib/encodings

rm android-sources/assets/lib/*.fas

cp $ECL_ANDROID/lib/ecl-21.2.1/*.asd       android-sources/assets/lib/
cp $ECL_ANDROID/lib/ecl-21.2.1/*.fas       android-sources/assets/lib/
cp $ECL_ANDROID/lib/ecl-21.2.1/*.doc       android-sources/assets/lib/
cp $ECL_ANDROID/lib/ecl-21.2.1/encodings/* android-sources/assets/lib/encodings/

# strip

$ANDROID_NDK_TOOLCHAIN/bin/aarch64-linux-android-strip android-sources/assets/lib/*.fas

# a Swank version (with a small patch) that is guaranteed to work on android

mkdir -p android-sources/assets/lib/quicklisp/local-projects

rm -fr            android-sources/assets/lib/quicklisp/local-projects/slime
cp -r ../../slime android-sources/assets/lib/quicklisp/local-projects/
