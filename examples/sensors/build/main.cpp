#undef SLOT

#include "main.h"
#include <ecl/ecl.h>
#include <eql5/eql.h>
#include <QApplication>
#include <QLabel>
#include <QTextCodec>
#include <QFileInfo>

extern "C" {
    void ini_app(cl_object);
}

QT_BEGIN_NAMESPACE

CBridge::CBridge(QObject* parent, const QString& name) : QObject(parent) {
    setObjectName(name); }

int main(int argc, char** argv) {

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication qapp(argc, argv);
    CBridge c_bridge(&qapp, "c_bridge");
    //qApp->setOrganizationName("MyTeam");
    //qApp->setOrganizationDomain("my-team.org");
    qApp->setApplicationName(QFileInfo(qApp->applicationFilePath()).baseName());

    QTextCodec* utf8 = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(utf8);

    // splash pixmap (see "../../../img/logo.png")
    QLabel* splash = new QLabel;
    splash->setPixmap(QPixmap(":/img/logo.png"));
    splash->setAlignment(Qt::AlignCenter);
    splash->show();
    qApp->processEvents();
    splash->deleteLater();

    EQL eql;
    // we need a fallback restart for connections from Slime
    eql.exec(ini_app, "(loop (with-simple-restart (restart-qt-events \"Restart Qt event processing.\") (qexec)))");

    return 0; }

QT_END_NAMESPACE
