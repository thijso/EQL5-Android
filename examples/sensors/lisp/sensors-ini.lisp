(qrequire :quick)

(in-package :sensors)

(defun start ()
  (eval:ini)
  (qml:ini-quick-view "qml/sensors.qml")
  (qconnect qml:*quick-view* "statusChanged(QQuickView::Status)" ; for reloading
            (lambda (status)
              (case status
                (#.|QQuickView.Ready|
                 (qml-reloaded))
                (#.|QQuickView.Error|
                 (qmsg (x:join (mapcar '|toString| (|errors| *quick-view*))
                               #.(make-string 2 :initial-element #\Newline)))))))
  (qlater 'ini)
  (qlater (lambda () (eval:eval-in-thread "(help)" nil)))) ; show help

;;; REPL

(defun show-repl (show) ; called from QML
  (when show
    (q> |opacity| ui:*repl-container* 0)
    (q> |visible| ui:*repl-container* t))
  (dotimes (n 10)
    (q> |opacity| ui:*repl-container* (/ (if show (1+ n) (- 9 n)) 10))
    (qsleep 0.015))
  (unless show
    (q> |visible| ui:*repl-container* nil))
  ;; sensor processing would interfere with REPL (on some devices)
  (if show
      (stop-sensor-timers)
      (start-sensor-timers)))

(defun reload-qml (&optional (url "http://localhost:8080/"))
  ;; please see README-1.md in REPL example
  "Reload QML file from an url, directly on the device."
  (let ((src (|toString| (|source| qml:*quick-view*))))
    (if (x:starts-with "qrc:/" src)
        (|setSource| qml:*quick-view* (qnew "QUrl(QString)"
                                            (x:string-substitute url "qrc:/" src)))
        (qml:reload))
    (|toString| (|source| qml:*quick-view*))))

(defun qml-reloaded ()
  (ini))
