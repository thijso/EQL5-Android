
*You probably want the new, cross-platform version of this, see*
[LQML](https://gitlab.com/eql/lqml).



### Info

This is an android port of [EQL5](https://gitlab.com/eql/EQL5).

See [README-PREPARE](README-PREPARE.md) for getting started.

License: MIT
